var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < 128; i++) {
    fill((1 - (i / 32)) * 255 * ((i + 1) % 2))
    noStroke()
    push()
    translate(windowWidth * 0.5, windowHeight * 0.5)
    rotate(i * 0.0175 * Math.PI * sin(frameCount * 0.05))
    rect(0, 0, boardSize * pow(0 + 0.95 * sin(frameCount * 0.005), (i + 1)), boardSize * pow(0 + 0.95 * sin(frameCount * 0.005), (i + 1)))
    pop()
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = j * numRows + i
      }
    }
    array[i] = columns
  }
  return array
}
